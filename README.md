bash_swarm pipeline
======================

# Table of contents

1. [Introduction](#1-introduction)
2. [Installation](#2-installation)
3. [Reporting bugs](#3-reporting-bugs)
4. [Running the pipeline](#4-running-the-pipeline)
5. [Results](#5-results)

-----------------

# 1. Introduction

Here, we reproduce the [Fred's metabarcoding pipeline](https://github.com/frederic-mahe/swarm/wiki/Fred%27s-metabarcoding-pipeline) to generate species environmental presence from raw eDNA data. This pipeline is based on [swarm](https://github.com/torognes/swarm) a robust and fast clustering method for amplicon-based studies.


# 2. Installation

In order to run "bash_swarm", you need a couple of programs. The
programs and libraries you absolutely need are :

- [swarm 2.1.6](https://github.com/torognes/swarm)

- [vsearch 2.6.2](https://github.com/torognes/vsearch)

- [python 2.7.12](https://www.python.org/downloads/release/python-2712/)

- [python 3.6.6](https://www.python.org/downloads/release/python-366/)

- [cutadapt 1.16](https://cutadapt.readthedocs.io/en/stable/installation.html)

- [GNU Parallel](https://www.gnu.org/software/parallel/)


In addition, you will need a *reference database* for taxonomic assignment. This reference database is a STAMPA FASTA file with sequence and related taxonomy. You can build such a reference database by following the instructions [here](http://gitlab.mbb.univ-montp2.fr/edna/reference_database).

# 3. Reporting bugs

If you're sure you've found a bug — e.g. if one of my programs crashes
with an obscur error message, or if the resulting file is missing part
of the original data, then by all means submit a bug report.

I use [GitLab's issue system](https://gitlab.mbb.univ-montp2.fr/edna/bash_swarm/issues)
as my bug database. You can submit your bug reports there. Please be as
verbose as possible — e.g. include the command line, etc


# 4. Running the pipeline

* open a shell
* make a folder, name it yourself, I named it workdir
```
mkdir workdir
cd workdir
```
* clone the project and switch to the main folder, it's your working directory
```
git clone http://gitlab.mbb.univ-montp2.fr/edna/bash_swarm.git
cd bash_swarm
```

* run the pipeline :
```
bash pipeline.sh /path/to/data /path/to/reference_database_stampa.fasta 16

```
order of arguments is important : 
1. absolute path to the folder which contains paired-end raw reads files and sample description file. This folder contains pairend-end raw reads `.fastq.gz` files and the sample description `.dat` files. Raw reads files from the same pair must be named as `*_R1.fastq.gz` and `*_R2.fastq.gz` where wildcard `*` is the name of the sequencing run. The alphanumeric order of the names of sample description `.dat` files must be the same than the names of paired-end raw reads `.fastq.gz` files. The sample description file is a text file where each line describes one sample. Columns are separated by space or tab characters. Sample description file is described [here](https://pythonhosted.org/OBITools/scripts/ngsfilter.html).
2. absolute path to the *reference database* for taxonomic assignment. This reference database is a STAMPA FASTA file with sequence and related taxonomy. You can build such a reference database by following the instructions [here](http://gitlab.mbb.univ-montp2.fr/edna/reference_database).
3. number of available cores (e.g : 16 cores)

# 5. Results

* [scripts](scripts) : contains required scripts by [main.sh](main.sh)

* [main](main) : contains intermediate files

* [assignations](assignations) : contains taxon-assigned OTU-representative sequences

* [final](final) : contains all the matrix species/sample