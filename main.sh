# define global variable
# From the config file

source infos/config.sh

## folder which contains .fastq data files and sample description .dat files
#FOLDER_FASTQ=$1
## absolute path to the stampa fasta file of the reference database
#REF_DATABASE=$2
## number of cores available
#CORES=$3
## list fastq files

######################################################################
##                       STEP 1                                     ##
######################################################################

for i in `ls "$FOLDER_FASTQ"*_R1.fastq.gz`;
do
echo $i | cut -d "." -f 1 | sed 's/_R1//g'
done > liste_fq
##list sample description files .dat
for i in `ls "$FOLDER_FASTQ"*dat`;
do
echo $i
done > liste_dat
## table of fastq and corresponding dat files
paste liste_fq liste_dat > liste_fq_dat
rm liste_fq liste_dat
## writing script bash : commands to apply on fastq/dat files
while IFS= read -r var
do
echo "bash scripts/filter_and_demultiplex.sh "$var" "$CORES
done < liste_fq_dat > fq_dat_cmd.sh
## run in parallel all commands
parallel < fq_dat_cmd.sh

######################################################################
##                       STEP 2                                     ##
######################################################################

# Clustering with swarm
bash scripts/cluster_and_assignment.sh

######################################################################
##                       STEP 3                                     ##
######################################################################

# Taxonomical assignement
bash scripts/assignations_ecotag.sh $FOLDER_FASTQ $REF_DATABASE
