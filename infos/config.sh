########################################################

## path to folder of "rapidrun" data folder
# test dataset
FOLDER_FASTQ="/media/superdisk/edna/donnees/test/tiny_rhone_miseq/" # Make sure there is a / at the end
REF_DATABASE="/media/superdisk/edna/donnees/reference_database/reference_database_teleo"
PERSO_DATABASE="" # Incorporate a custom database to ecotag. Ifelse statement. 
## number of available cores
CORES=32
# Project Name
PROJET="Rhone"

###############################################################################
## singularity containers path
###############################################################################

EDNATOOLS_SIMG="/media/superdisk/utils/conteneurs/ednatools.simg"
OBITOOLS_SIMG="/media/superdisk/utils/conteneurs/obitools.simg"

## singularity exec command
# SINGULARITY_EXEC_CMD="singularity exec -B .:"${SING_MNT}
SINGULARITY_EXEC_CMD="singularity exec --bind /media/superdisk:/media/superdisk"

###############################################################################
## softwares
#### obitools
ecotag=${SINGULARITY_EXEC_CMD}" "${OBITOOLS_SIMG}" ecotag"
obiannotate=${SINGULARITY_EXEC_CMD}" "${OBITOOLS_SIMG}" obiannotate"
obisort=${SINGULARITY_EXEC_CMD}" "${OBITOOLS_SIMG}" obisort"
obitab=${SINGULARITY_EXEC_CMD}" "${OBITOOLS_SIMG}" obitab"
### ednatools
vsearch=${SINGULARITY_EXEC_CMD}" "${EDNATOOLS_SIMG}" vsearch"
cutadapt=${SINGULARITY_EXEC_CMD}" "${EDNATOOLS_SIMG}" cutadapt"
swarm=${SINGULARITY_EXEC_CMD}" "${EDNATOOLS_SIMG}" swarm"
python2_edna=${SINGULARITY_EXEC_CMD}" "${EDNATOOLS_SIMG}" python2"
