# REMOVE: IT IS STAMPA

## define global variables
### absolute path of the stampa fasta reference database file
DATABASE=$1
### absolute path of the vsearch representatives sequences file
QUERIES=$2
### prefix of the project
pref=$3
### number of available cores
THREADS=$4
### [vsearch parameter] reject if identity lower
IDENTITY="0.5"
### [vsearch parameter] number of non-matching hits to consider
MAXREJECTS=32
NULL="/dev/null"
### absolute path to the stampa fasta assigned representative sequences file
ASSIGNMENTS=$5
### dereplicated representatives sequences
VSEARCH_ASSIGNMENTS="${ASSIGNMENTS/.fas/.tmp.fas}"

## Dereplicate and assignation (vsearch)
vsearch --usearch_global "${QUERIES}" \
--threads "${THREADS}" \
--dbmask none \
--qmask none \
--rowlen 0 \
--notrunclabels \
--userfields query+id1+target \
--maxaccepts 0 \
--maxrejects "${MAXREJECTS}" \
--top_hits_only \
--output_no_hits \
--db "${DATABASE}" \
--id "${IDENTITY}" \
--iddef 1 \
--userout "${VSEARCH_ASSIGNMENTS}" > "${NULL}" 2> "${NULL}"

## convert assigned representatives sequence vsearch file into stampa fasta file
python3 scripts/convert_vsearch_into_stampa.py -f "${VSEARCH_ASSIGNMENTS}" -o "${ASSIGNMENTS}"
