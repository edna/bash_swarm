source infos/config.sh

# Start here
FOLDER=$1 # Fakarava_Chond
base_dir=$2 # /media/superdisk/edna/donnees/reference_database_chond
PROJET=$PROJET

# Other
mkdir assignations/intermediaire
pref_bdr="std"
main_dir=$(pwd)/main
fin_dir=$(pwd)/final
assignation_dir=$(pwd)/assignations

# Convert .table to fasta for Taxonomical assignation
Rscript scripts/R_table_to_fasta.R

# Sur genbank
$ecotag -d "$base_dir"/embl_"$pref_bdr" -R $base_dir/db_embl_"$pref_bdr".fasta "$fin_dir"/*fasta  > "$assignation_dir"/intermediaire/"$PROJET"_OTUs_tag_genbank.fasta

##Some unuseful attributes can be removed at this stage
$obiannotate  --delete-tag=scientific_name_by_db --delete-tag=obiclean_samplecount \
 --delete-tag=obiclean_count --delete-tag=obiclean_singletoncount \
 --delete-tag=obiclean_cluster --delete-tag=obiclean_internalcount \
 --delete-tag=obiclean_head --delete-tag=obiclean_headcount \
 --delete-tag=id_status --delete-tag=rank_by_db --delete-tag=obiclean_status \
 --delete-tag=seq_length_ori --delete-tag=sminL --delete-tag=sminR \
 --delete-tag=reverse_score --delete-tag=reverse_primer --delete-tag=reverse_match --delete-tag=reverse_tag \
 --delete-tag=forward_tag --delete-tag=forward_score --delete-tag=forward_primer --delete-tag=forward_match \
 --delete-tag=tail_quality "$assignation_dir"/intermediaire/"$PROJET"_OTUs_tag_genbank.fasta > "$assignation_dir"/intermediaire/"$PROJET"_OTUs_tag_ann_genbank.fasta
##The sequences can be sorted by decreasing order of count
$obisort -k count -r "$assignation_dir"/intermediaire/"$PROJET"_OTUs_tag_ann_genbank.fasta > "$assignation_dir"/intermediaire/"$PROJET"_OTUs_tag_ann_sort_genbank.fasta
##generate a table final results
$obitab -o "$assignation_dir"/intermediaire/"$PROJET"_OTUs_tag_ann_sort_genbank.fasta > "$assignation_dir"/"$PROJET"_OTUs_ecotag_genbank.csv
