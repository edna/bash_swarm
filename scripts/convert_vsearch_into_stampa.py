#!/share/reservebenefit/TEMP/bin/python/bin/python
# REMOVE: IT IS STAMPA
#===============================================================================
#INFORMATIONS
#===============================================================================
"""
CEFE - EPHE - YERSIN 2018
guerin pierre-edouard
convertir sortie vsearch en entree stampa
"""
#===============================================================================
#USAGES
#===============================================================================
"""
input :
un fichier output de vsearch
python3

"""
#===============================================================================
#MODULES
#===============================================================================
import Bio
from Bio import SeqIO
from Bio.Alphabet import IUPAC
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
import argparse
from ete3 import NCBITaxa
#===============================================================================
#CLASSES
#===============================================================================
class Otu:
    def __init__(self,nom,abundance,identity,taxo,ref):
        self.nom=nom
        self.abundance=abundance
        self.identity=identity
        self.taxo=taxo
        self.reflist=""
        self.reflist+=str(ref)
    def add_ref(self,ref2):
        self.reflist+=','
        self.reflist+=str(ref)

#===============================================================================
#ARGUMENTS
#===============================================================================

parser = argparse.ArgumentParser(description='convert vsearch to stampa format')
parser.add_argument("-o","--output", type=str)
parser.add_argument("-f","--fasta",type=str)


#===============================================================================
#MAIN
#===============================================================================
args = parser.parse_args()
outputFile = args.output
fastaFile = args.fasta
list_otu=[]
with open(fastaFile,'r') as fastaf:
    for line in fastaf.readlines():
        lineSplit=line.split(';')
        nom=lineSplit[0]
        size=lineSplit[1]
        resteligne=lineSplit[2]
        abundance=size.split('=')[1]
        resteligneSplit=resteligne.split()
        if int(len(resteligneSplit))<3:
            identity=0
            ref="NA"
            taxo="NA"
        else:
            identity=resteligneSplit[0]
            ref=resteligneSplit[1]
            taxo=resteligneSplit[2]
        if nom in [otu.nom for otu in list_otu]:
            for otu in list_otu:
                if nom == otu.nom:
                    otu.add_ref(ref)
        else:
            local_otu=Otu(nom,abundance,identity,taxo,ref)
            list_otu.append(local_otu)
fastaf.closed
outputOpen=open(outputFile,'w')
for otu in list_otu:
    ligne=str(otu.nom)+"\t"+str(otu.abundance)+"\t"+str(otu.identity)+"\t"+str(otu.taxo)+"\t"+str(otu.reflist)+"\n"
    outputOpen.write(ligne)
