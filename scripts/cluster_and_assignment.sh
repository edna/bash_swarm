source infos/config.sh

## define global variables
### prefix of the project
pref=$PROJET

### local working directory
#### where to store intermediate files
main_dir=$(pwd)/main
#### where to store final results
fin_dir=$(pwd)/final
#### merged directory
merged_dir=$(pwd)/merged
#### where to store assignations
assignation_dir=$(pwd)/assignations
#### where to store demultiplexed samples
sample_dir=$(pwd)/samples
#### reference database stampa fasta file absolute path
DATABASE_REF=$REF_DATABASE

## Pool sequences
ALL_CAT_FASTA=$main_dir/"$pref".list_fasta
cat $sample_dir/*fas > "${ALL_CAT_FASTA}"
ALL_QUALITY=$main_dir/"$pref".qual
## Pool quality
cat $merged_dir/*qual > "${ALL_QUALITY}"
## Dereplicate (vsearch)
ALL_UNIQ_FASTA=$main_dir/"$pref".all_samples_uniq.fas
$vsearch --derep_fulllength "${ALL_CAT_FASTA}" \
             --sizein \
             --sizeout \
             --fasta_width 0 \
             --output "${ALL_UNIQ_FASTA}" > /dev/null

## fix name of sequences (have to end with ';')
sed -i '1~2 s/$/;/g' "${ALL_UNIQ_FASTA}"

## Clustering
TMP_REPRESENTATIVES=$main_dir/"$pref".tmp_representative
STATS=${ALL_UNIQ_FASTA/.fas/_1f.stats}
SWARMS=${ALL_UNIQ_FASTA/.fas/_1f.swarms}
$swarm \
    -d 1 -f -t ${CORES} -z \
    -i ${ALL_UNIQ_FASTA/.fas/_1f.struct} \
    -s ${STATS} \
    -w ${TMP_REPRESENTATIVES} \
    -o ${SWARMS} < ${ALL_UNIQ_FASTA}
## Sort representatives
$vsearch --fasta_width 0 \
             --sortbysize ${TMP_REPRESENTATIVES} \
             --output ${ALL_UNIQ_FASTA/.fas/_1f_representatives.fas}
## Chimera checking
REPRESENTATIVES=${ALL_UNIQ_FASTA/.fas/_1f_representatives.fas}
UCHIME=${REPRESENTATIVES/.fas/.uchime}
$vsearch --uchime_denovo "${REPRESENTATIVES}" \
             --uchimeout "${UCHIME}"

## TAXONOMIC ASSIGNMENT
#### NO ASSIGNMENT
## create fake assignment table with no taxonomic information
ASSIGNMENTS="${ALL_UNIQ_FASTA/.fasta/_1f_representatives.results}"
grep "^>" ${REPRESENTATIVES} | \
    sed 's/^>//
    s/;size=/\t/
    s/;$/\t100.0\tNA\tNA/' > ${ASSIGNMENTS}
## BUILD THE OTU TABLE
echo "build OTU tables"
OTU_TABLE=$fin_dir/"$pref".OTU.table
$python2_edna scripts/OTU_contingency_table.py \
    "${REPRESENTATIVES}" \
    "${STATS}" \
    "${SWARMS}" \
    "${UCHIME}" \
    "${ALL_QUALITY}" \
    "${ASSIGNMENTS}" \
    $sample_dir/*.fas > ${OTU_TABLE}

               #  ## TAXONOMIC ASSIGNMENT
               #  ASSIGNMENTS=$assignation_dir/"$pref".TAXO_representatives.fas
               #  bash scripts/assignation.sh "${DATABASE_REF}" "${REPRESENTATIVES}" "${pref}" "${CORES}" "${ASSIGNMENTS}"
               #  ## BUILD THE OTU TABLE
               #  OTU_TABLE=$fin_dir/"$pref".OTU.table
               #  $python2_edna scripts/OTU_contingency_table.py \
               #  "${REPRESENTATIVES}" \
               #  "${STATS}" \
               #  "${SWARMS}" \
               #  "${UCHIME}" \
               #  "${ALL_QUALITY}" \
               #  "${ASSIGNMENTS}" \
               #  $main_dir/*.fas > ${OTU_TABLE}
