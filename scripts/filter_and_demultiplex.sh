# Source the variables
source infos/config.sh

## define global variables
### Fastq paired-end file absolute path
R1_fastq=$1"_R1.fastq.gz"
R2_fastq=$1"_R2.fastq.gz"
### prefix of the run
pref=`echo $1 | rev | cut -d "/" -f1 | rev`
### sample description .dat file absolute path
sample_description_file=$2
### local working directory
#### where to store intermediate files
main_dir=$(pwd)/main
#### where to store final results
fin_dir=$(pwd)/final
#### where to store demultiplexed samples
sample_dir=$(pwd)/samples
#### where to store merged files
merged_dir=$(pwd)/merged
#### number of available cores
THREADS=$3
##Check quality encoding (33 or 64?)
#zcat ${R1_fastq} | sed -n 1,400p > $main_dir/subset.fastq
#vsearch --fastq_chars $main_dir/subset.fastq 2> $main_dir/subset.log
## Original Sanger format (phred+33)
ENCODING=33

## Merge read pairs
MERGED="$merged_dir"/"$pref".assembled.fastq
$vsearch \
 --threads ${THREADS} \
 --fastq_mergepairs ${R1_fastq} \
 --reverse ${R2_fastq} \
 --fastq_ascii ${ENCODING} \
 --fastqout ${MERGED} \
 --fastq_allowmergestagger \
 --quiet 2>> ${MERGED/.fastq/.log}

## Demultiplexing, primer clipping, sample dereplication and quality extraction
### global variables used for demultiplexing
INPUT=${MERGED}
TAGS=$sample_description_file
PRIMER_F=`awk '{ print $4 }' ${sample_description_file} | uniq`
PRIMER_R=`awk '{ print $5 }' ${sample_description_file} | uniq | tr "[ATGCUatgcuNnYyRrSsWwKkMmBbDdHhVv]" "[TACGAtacgaNnRrYySsWwMmKkVvHhDdBb]" | rev`
MIN_LENGTH=20
MAX_LENGTH=120
MIN_F=$(( ${#PRIMER_F} * 2 / 3 ))
MIN_R=$(( ${#PRIMER_R} * 2 / 3 ))
### Define binaries, temporary files and output files
CUTADAPT=${cutadapt}" --discard-untrimmed -m "${MIN_LENGTH}
TMP_FASTQ="$main_dir"/"$pref".tmp_fastq
TMP_FASTQ2="$main_dir"/"$pref".tmp_fastq2
TMP_FASTA="$main_dir"/"$pref".tmp_fasta
OUTPUT="$main_dir"/"$pref".tmp_output
QUALITY_FILE="${INPUT/.fastq/.qual}"
### it's necessary to generate reverse complement fastq due to cutadapt limitations
INPUT_REVCOMP="${INPUT/.fastq/_rev.fastq}"
$vsearch --quiet \
        --fastx_revcomp "${INPUT}" \
        --fastqout "${INPUT_REVCOMP}"
### demultiplexing...
awk '{ print $2"\t"$3}' ${TAGS} | while read TAG_NAME TAG_SEQ ; do
    LOG="$sample_dir"/"${TAG_NAME}".log
    FINAL_FASTA="$sample_dir"/"${TAG_NAME}".fas
    RTAG_SEQ=`echo $TAG_SEQ | tr "[ATGCUatgcuNnYyRrSsWwKkMmBbDdHhVv]" "[TACGAtacgaNnRrYySsWwMmKkVvHhDdBb]" | rev`
    #### Trim tags, forward & reverse primers (search normal and antisens)
    cat "${INPUT}" "${INPUT_REVCOMP}" | \
        ${CUTADAPT} -g "${TAG_SEQ}" -O "${#TAG_SEQ}" - 2> "${LOG}" | \
        ${CUTADAPT} -g "^""${PRIMER_F}" -O "${MIN_F}" - 2>> "${LOG}" | \
        ${CUTADAPT} -a "${RTAG_SEQ}" -O "${#RTAG_SEQ}" - 2>> "${LOG}" | \
        ${CUTADAPT} -a "${PRIMER_R}""$" -O "${MIN_R}" - 2>> "${LOG}" | \
        $cutadapt -M ${MAX_LENGTH} - 2>> "${LOG}" > "${TAG_NAME}"_tmp
    mv "${TAG_NAME}"_tmp "${TMP_FASTQ}"
    #### Discard sequences containing Ns, add expected error rates
    $vsearch \
        --quiet \
        --fastq_filter "${TMP_FASTQ}" \
        --fastq_maxns 0 \
        --relabel_sha1 \
        --eeout \
        --fastqout "${TMP_FASTQ2}" 2>> "${LOG}"
    #### Discard sequences containing Ns, convert to fasta
    $vsearch \
        --quiet \
        --fastq_filter "${TMP_FASTQ}" \
        --fastq_maxns 0 \
        --relabel_sha1 \
        --eeout \
        --fastqout "${TMP_FASTQ2}" 2>> "${LOG}"
    #### Discard sequences containing Ns, convert to fasta
    $vsearch \
        --quiet \
        --fastq_filter "${TMP_FASTQ}" \
        --fastq_maxns 0 \
        --fastaout "${TMP_FASTA}" 2>> "${LOG}"
    #### Dereplicate at the study level
    $vsearch \
        --quiet \
        --minseqlength $MIN_LENGTH \
        --derep_fulllength "${TMP_FASTA}" \
        --sizeout \
        --fasta_width 0 \
        --relabel_sha1 \
        --output "${FINAL_FASTA}" 2>> "${LOG}"
    #### Discard quality lines, extract hash, expected error rates and read length
    sed 'n;n;N;d' "${TMP_FASTQ2}" | \
        awk 'BEGIN {FS = "[;=]"}
             {if (/^@/) {printf "%s\t%s\t", $1, $3} else {print length($1)}}' | \
        tr -d "@" >> "${OUTPUT}"
done
## Produce the final quality file
sort -k3,3n -k1,1d -k2,2n "${OUTPUT}" | uniq --check-chars=40 > "${QUALITY_FILE}"
## Clean
rm -f "${TMP_FASTQ}" "${TMP_FASTA}" "${TMP_FASTQ2}" "${OUTPUT}"
